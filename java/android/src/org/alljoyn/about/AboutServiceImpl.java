/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.alljoyn.about.client.AboutClient;
import org.alljoyn.about.client.AboutClientImpl;
import org.alljoyn.about.icon.AboutIconClient;
import org.alljoyn.about.icon.AboutIconClientImpl;
import org.alljoyn.about.transport.AboutTransport;
import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusListener;
import org.alljoyn.bus.BusObject;
import org.alljoyn.bus.ErrorReplyBusException;
import org.alljoyn.bus.SignalEmitter;
import org.alljoyn.bus.Status;
import org.alljoyn.bus.Variant;
import org.alljoyn.bus.annotation.BusSignalHandler;
import org.alljoyn.services.common.AnnouncementHandler;
import org.alljoyn.services.common.BusObjectDescription;
import org.alljoyn.services.common.ServiceAvailabilityListener;
import org.alljoyn.services.common.ServiceCommonImpl;
import org.alljoyn.services.common.storage.PropertyStore;
import org.alljoyn.services.common.storage.PropertyStore.Filter;
import org.alljoyn.services.common.utils.TransportUtil;

public class AboutServiceImpl extends ServiceCommonImpl implements AboutService
{


	/********* Client *********/	
	// the AnnouncmentReceiver will specify the interface and signal names.
	private static final String ANNOUNCE_MATCH_RULE	          = "interface='" + ANNOUNCE_IFNAME + "'";


	private static AboutServiceImpl m_instance;
	private Vector<AnnouncementHandler> m_announcementHandlers;
	private BusListener m_busListeners;

	private AnnouncmentReceiver m_announcmentReceiver;

	/********* Sender *********/

	private PropertyStore m_propertyStore;
	private AboutTransport m_aboutInterface;
	private Announcer m_announcer;

	private AboutTransport m_announcementEmitter;
	private Set<BusObjectDescription> m_ObjectDescriptions = new HashSet<BusObjectDescription>();

	private short m_servicesPort;
	private String m_languageTag = "en";


	/********* General *********/

	public static AboutService getInstance()
	{
		if(m_instance == null)
			m_instance = new AboutServiceImpl();
		return m_instance;
	}

	private AboutServiceImpl() 
	{
		super();
		TAG = "ioe" + AboutServiceImpl.class.getSimpleName();
		m_announcementHandlers = new Vector<AnnouncementHandler>();
	}


	//	======================
	/********* Client *********/

	@Override
	public void startAboutClient(/*String appName, */BusAttachment bus/*, AnnouncementHandler receiver*/) throws Exception 
	{
		//		startAJ(appName);
		//		m_announcementHandler = receiver;
		super.startClient();
		setBus(bus);
		registerDeviceListener();
		registerAnnouncementReceiver();
	}

	public void addAnnouncementHandler(AnnouncementHandler handler)
	{
		m_announcementHandlers.add(handler);
	}

	public void removeAnnouncementHandler(AnnouncementHandler handler)
	{
		m_announcementHandlers.remove(handler);
	}

	private void registerDeviceListener()
	{
		m_busListeners = new BusListener() 
		{
			@Override
			public void foundAdvertisedName(final String name, short transport, String namePrefix) 
			{
				getLogger().debug(TAG, "MyBusListener.foundAdvertisedName"+ name);
				// Do nothing. devices are discovered by Announcements
			}

			public void nameOwnerChanged(String busName, String previousOwner, String newOwner)
			{
				getLogger().debug(TAG,String.format("MyBusListener.nameOwnerChanged(%s, %s, %s)", busName, previousOwner, newOwner));
			}

			public void lostAdvertisedName(String name, short transport, String namePrefix)
			{
				getLogger().debug(TAG, String.format("MyBusListener.lostAdvertisedName(%s, 0x%04x, %s)", name, transport, namePrefix));
				for (int i = 0; i < m_announcementHandlers.size(); i++)
				{
					AnnouncementHandler current = m_announcementHandlers.elementAt(i);
					current.onDeviceLost(name);
				}
			}
		};
		getBus().registerBusListener(m_busListeners);
		Status status = getBus().findAdvertisedName(":");
	}

	private void registerAnnouncementReceiver()
	{
		// for announce
		m_announcmentReceiver = new AnnouncmentReceiver();
		Status status = getBus().registerBusObject(m_announcmentReceiver,  AboutTransport.OBJ_PATH);
		getLogger().info(TAG, "BusAttachment.registerBusObject(AnnouncmentReceiver) status = " + status);

		status = getBus().registerSignalHandlers(m_announcmentReceiver);
		getLogger().info(TAG, "BusAttachment.registerSignalHandlers(AnnouncmentReceiver) status = " + status);

		status = getBus().addMatch(ANNOUNCE_MATCH_RULE);
		getLogger().info(TAG, "BusAttachment.addMatch() status = " + status);
	}

	private class AnnouncmentReceiver implements BusObject, AboutTransport
	{
		@Override
		@BusSignalHandler(iface = ANNOUNCE_IFNAME, signal = SIGNAL_NAME)
		public void Announce(short version, short port, BusObjectDescription[] objectDescriptions,
				Map<String, Variant> serviceMetadata) 
		{

			Map<String, Object> aboutMap;
			try
			{
				aboutMap = TransportUtil.fromVariantMap(serviceMetadata);
				String sender = getBus().getMessageContext().sender;

				for (int i = 0; i < m_announcementHandlers.size(); i++)
				{
					AnnouncementHandler current = m_announcementHandlers.elementAt(i);
					current.onAnnouncement(sender, port, objectDescriptions, aboutMap);
				}

				//m_receiver.onAnnouncement(deviceUniqueId, objectDescriptions, aboutMap);
			} catch (BusException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		/**                                                                                                                          
		 * Intentionally empty implementation. Since class is only used as a signal handler, it will never be called directly.
		 */

		@Override
		public short getVersion() throws BusException
		{
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public short getPort() throws BusException
		{
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public Map<String, Variant> GetAboutData(String languageTag)
				throws BusException
				{
			// TODO Auto-generated method stub
			return null;
				}

		@Override
		public BusObjectDescription[] GetObjectDescription()
				throws BusException
				{
			// TODO Auto-generated method stub
			return null;
				}
	}

	@Override
	public void stopAboutClient() throws Exception 
	{
		if (getBus() != null)
		{
			if (m_announcmentReceiver != null)
			{
				getBus().unregisterSignalHandlers(m_announcmentReceiver);
			}
			getBus().unregisterBusObject(m_announcmentReceiver);
			getBus().unregisterBusListener(m_busListeners);
			m_announcementHandlers.removeAllElements();
			m_announcmentReceiver = null;
			m_busListeners = null;
		}
		super.stopClient();
		//		m_bus.removeMatch(SESSIONLESS_MATCH_RULE);
	}

	@Override
	public AboutClient createAboutClient(String peerName, ServiceAvailabilityListener serviceAvailabilityListener, short port) throws Exception
	{
		return  new AboutClientImpl(peerName, getBus(), serviceAvailabilityListener, port);
	}

	@Override
	public AboutIconClient createAboutIconClient(String peerName,ServiceAvailabilityListener serviceAvailabilityListener, short port)
			throws BusException {

		return  new AboutIconClientImpl(peerName, getBus(), serviceAvailabilityListener, port);
	}
	//	======================
	/**
	 * **************** Server ************** 
	 */

	@Override
	public void startAboutServer(
			short port,
			/*String appName,*/
			PropertyStore propertyStore, BusAttachment bus) throws Exception
			{
		//		m_aboutMap = new HashMap<String, Object>(aboutValues);
		//		m_configMap = new HashMap<String, Object>(configValues);

		super.startServer();
		m_propertyStore = propertyStore;
		m_servicesPort = port;
		setBus(bus);
		//		startAJ(appName);

		//createAnnouncer();
		registerAboutInterface();

		addObjectDescription(AboutTransport.OBJ_PATH, new String [] {AboutTransport.INTERFACE_NAME});
		//		bindSessionPort(port);


		//		getAnnouncer().announce();
			}

	private void createAnnouncer() throws Exception
	{
		if (m_announcer == null)
		{
			m_announcer = new Announcer()
			{

				private boolean m_isAnnouncing = true;

				/**
				 * Announce the about table. Whoever called addMatch("sessionless='t',interface='org.alljoyn.About'") will be notified
				 */
				@Override
				public void announce()
				{
					if (m_announcementEmitter != null && isAnnouncing())
					{
						BusObjectDescription[] objectDescriptionArray = m_ObjectDescriptions
								.toArray(new BusObjectDescription[] {});
						Map<String, Object> announceMap = m_propertyStore.ReadAll(m_languageTag, Filter.ANNOUNCE);
						Map<String, Variant> variantAboutMap = TransportUtil
								.toVariantMap(announceMap);
						m_announcementEmitter.Announce((short)PROTOCOL_VERSION, m_servicesPort, objectDescriptionArray,
								variantAboutMap);
					}
				}

				@Override
				public boolean isAnnouncing()
				{
					return m_isAnnouncing;
				}

				@Override
				public void setAnnouncing(boolean enable)
				{
					m_isAnnouncing = enable;
				}

				@Override
				public void addBusObjectAnnouncements(
						List<BusObjectDescription> descriptions)
				{
					m_ObjectDescriptions.addAll(descriptions);
					// notify AllJoyn peers
					/*if (m_announcer != null)
					{
						m_announcer.announce();
					}*/
				}

				@Override
				public void removeBusObjectAnnouncements(
						List<BusObjectDescription> descriptions)
				{

					for (BusObjectDescription removeMe : descriptions)
					{

						BusObjectDescription match = null;
						for (BusObjectDescription boDescription : m_ObjectDescriptions)
						{
							if (removeMe.getPath().equalsIgnoreCase(
									boDescription.getPath()))
							{
								if (removeMe.getInterfaces().length == 0)
								{
									// remove all interfaces => remove busobject
									match = boDescription;
									break;
								} else
								{
									if (Arrays.equals(removeMe.getInterfaces(),
											boDescription.getInterfaces()))
									{
										// remove all interfaces => remove busobject
										match = boDescription;
										break;
									} else
									{
										// keep bus object, purge interfaces
										List<String> newInterfaces = new ArrayList<String>(
												Arrays.asList(boDescription.interfaces));
										newInterfaces.removeAll(Arrays
												.asList(removeMe
														.getInterfaces()));
										boDescription
										.setInterfaces(newInterfaces
												.toArray(new String[] {}));
									}
								}
							}
						}

						if (match != null)
						{
							m_ObjectDescriptions.remove(match);
						}
					}

				}
			};

			SignalEmitter emitter = new SignalEmitter(m_aboutInterface,
					SignalEmitter.GlobalBroadcast.Off);
			emitter.setSessionlessFlag(true);
			emitter.setTimeToLive(0);
			m_announcementEmitter = emitter.getInterface(AboutTransport.class);
		}		

	}

	private void registerAboutInterface() throws Exception
	{
		m_aboutInterface = new AboutInterface();

		Status status = getBus().registerBusObject(m_aboutInterface, AboutTransport.OBJ_PATH);
		getLogger().debug(TAG, String.format("BusAttachment.registerBusObject(m_aboutConfigInterface): %s", status));
		if (status != Status.OK) {
			return;
		}
	}
	@Override
	public void addObjectDescription(String objPath, String [] interfaces)
	{
		//		m_configInterface = new ConfigInterface();

		// add self. announcer isn't ready yet, so no announcement will go
		List<BusObjectDescription> addBusObjectDescriptions = new ArrayList<BusObjectDescription>(2);
		BusObjectDescription ajAboutBusObjectDescription = new BusObjectDescription();
		ajAboutBusObjectDescription.setPath(objPath);
		ajAboutBusObjectDescription.setInterfaces(interfaces);
		addBusObjectDescriptions.add(ajAboutBusObjectDescription);

		getAnnouncer().addBusObjectAnnouncements(addBusObjectDescriptions);
	}

	@Override
	public void addObjectDescriptions(List<BusObjectDescription> addBusObjectDescriptions)
	{
		getAnnouncer().addBusObjectAnnouncements(addBusObjectDescriptions);
	}

	@Override
	public void removeObjectDescription(String objPath, String[] interfaces) {

		List<BusObjectDescription> addBusObjectDescriptions = new ArrayList<BusObjectDescription>(2);
		BusObjectDescription busObjectDescription = new BusObjectDescription();
		busObjectDescription.setPath(objPath);
		busObjectDescription.setInterfaces(interfaces);
		addBusObjectDescriptions.add(busObjectDescription);

		getAnnouncer().removeBusObjectAnnouncements(addBusObjectDescriptions);
	}

	@Override
	public void removeObjectDescriptions(List<BusObjectDescription> removeBusObjectDescriptions)
	{
		getAnnouncer().removeBusObjectAnnouncements(removeBusObjectDescriptions);
	}

	/*private String bindSessionPort(final short port)
	{

	 * Create a new session listening on the contact port of the about/config service.

		Mutable.ShortValue contactPort = new Mutable.ShortValue(port);

		SessionOpts sessionOpts = new SessionOpts();
		sessionOpts.traffic = SessionOpts.TRAFFIC_MESSAGES;
		sessionOpts.isMultipoint = true;
		sessionOpts.proximity = SessionOpts.PROXIMITY_ANY;
		sessionOpts.transports = SessionOpts.TRANSPORT_ANY;

		Status status = m_bus.bindSessionPort(contactPort, sessionOpts, new SessionPortListener() {
			@Override
			public boolean acceptSessionJoiner(short sessionPort, String joiner, SessionOpts sessionOpts) {
				if (sessionPort == port) {
					return true;
				} else {
					return false;
				}
			}
			public void sessionJoined(short sessionPort, int id, String joiner){
				getLogger().info(TAG, String.format("SessionPortListener.sessionJoined(%d, %d, %s)", sessionPort, id, joiner));

			}
		});

		String logMessage = String.format("BusAttachment.bindSessionPort(%d, %s): %s",
				contactPort.value, sessionOpts.toString(),status);
		getLogger().debug(TAG, logMessage);
		if (status != Status.OK) {
			return "";
		}

		String serviceName = m_bus.getUniqueName();

		status = m_bus.advertiseName(serviceName, SessionOpts.TRANSPORT_ANY);
		getLogger().debug(TAG, String.format("BusAttachement.advertiseName(%s): %s", serviceName, status));

		return serviceName;
	}*/


	@Override
	public void stopAboutServer() throws Exception 
	{
		if(getBus() != null)
		{
			if (m_aboutInterface != null)
			{
				getBus().unregisterBusObject(m_aboutInterface);
			}
			m_ObjectDescriptions.clear();
			
			m_announcementEmitter = null;
			m_announcer = null;
		}
		
		super.stopServer();
		//		String serviceName = constructServiceName();
		//		Status status = m_bus.releaseName(serviceName);
	}

	public Announcer getAnnouncer()
	{
		try {
			createAnnouncer();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} // will create new only if null.
		return m_announcer;
	}

	/**
	 * The AllJoyn BusObject that exposes the About and Config Interfaces of this device over the Bus.
	 * One Bus Object represents 2 interfaces. 
	 * @author dzehavi
	 *
	 */
	private class AboutInterface implements BusObject, AboutTransport 
	{

		@Override
		public Map<String, Variant> GetAboutData(String languageTag) throws BusException
		{
			Map<String, Object> about = m_propertyStore.ReadAll(languageTag, Filter.READ);
			if (about == null)
			{
				// throw an error
				throw new ErrorReplyBusException("org.alljoyn.Error.LanguageNotSupported","The language specified is not supported");
			}
			return TransportUtil.toVariantMap(about);
		}

		@Override
		public BusObjectDescription[] GetObjectDescription()
				throws BusException
				{
			return m_ObjectDescriptions.toArray(new BusObjectDescription[]{});
				}

		@Override
		public short getVersion() throws BusException
		{
			return PROTOCOL_VERSION;
		}

		@Override
		public short getPort() throws BusException
		{
			return m_servicesPort;
		}

		/**                                                                                                                          
		 * Intentionally empty implementation of ServiceAnnouncement method.  
		 * Since this method is only used as a signal emitter, it will never be called directly.
		 */

		@Override
		public void Announce(short version, short port, BusObjectDescription[] objectDescriptions,
				Map<String, Variant> serviceMetadata)
		{}

	}


	@Override
	public List<BusObjectDescription> getBusObjectDescriptions() {
		// add self. announcer isn't ready yet, so no announcement will go
		List<BusObjectDescription> addBusObjectDescriptions = new ArrayList<BusObjectDescription>(2);
		BusObjectDescription ajAboutBusObjectDescription = new BusObjectDescription();
		ajAboutBusObjectDescription.setPath(AboutTransport.OBJ_PATH);
		ajAboutBusObjectDescription.setInterfaces(new String[]{AboutTransport.INTERFACE_NAME});
		addBusObjectDescriptions.add(ajAboutBusObjectDescription);
		return addBusObjectDescriptions;
	}

	@Override
	public void announce() {
		getAnnouncer().announce();
	}
	//========================

}
