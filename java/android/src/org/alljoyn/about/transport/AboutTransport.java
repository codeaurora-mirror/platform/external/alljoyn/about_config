/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

/**
 * 
 */
package org.alljoyn.about.transport;

import java.util.Map;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusObject;
import org.alljoyn.bus.Variant;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusMethod;
import org.alljoyn.bus.annotation.BusProperty;
import org.alljoyn.bus.annotation.BusSignal;
import org.alljoyn.services.common.BusObjectDescription;

/**
 * @author dzehavi
 *
 */
@BusInterface (name = AboutTransport.INTERFACE_NAME)
public interface AboutTransport extends BusObject
{
	public static final String INTERFACE_NAME = "org.alljoyn.About";
	public final static String OBJ_PATH = "/About";

	/**
	 * @return the version of the protocol
	 * @throws BusException
	 */
    @BusProperty(signature="q")
    public short getVersion() throws BusException;

	/**
	 * @return the port for the sessions to the services
	 * @throws BusException
	 */
    @BusProperty(signature="q")
    public short getPort() throws BusException;

    /**
	 * 
	 * @param languageTag IETF language tags specified by  RFC 5646
	 * @return all the configuration fields based on the language tag.  See The list of known configuration fields in About interface for more details.
	 * @throws BusException
	 */
    @BusMethod(signature = "s", replySignature="a{sv}")
    public Map<String, Variant> GetAboutData(String languageTag) throws BusException;

    /**
     * Return the array of object paths and the list of all interfaces available at the given object path.
     * @return the array of object paths and the list of all interfaces available at the given object path.
     * @throws BusException
     */
    @BusMethod(replySignature="a(oas)")
    public BusObjectDescription[] GetObjectDescription() throws BusException;

	/**
     * This signal is used to announce the list of all interfaces available at the given object path.
     * @param version The interface version is added since it might help with the decision to connect back.
     * @param port The global gateway port for the services for this application
     * @param objectDescriptions Descriptions of the BusObjects that make up the service. "a(sas)"
     * @param serviceMetadata Service specific key/value pairs. (Service implementers are free to populate this dictionary with any key/value pairs that are meaningful to the service and its potential consumers) "a{sv}" 
     */
    @BusSignal (signature="qqa(oas)a{sv}")
    public void Announce(short version, short port, BusObjectDescription[] objectDescriptions, Map<String,Variant> serviceMetadata);

}
