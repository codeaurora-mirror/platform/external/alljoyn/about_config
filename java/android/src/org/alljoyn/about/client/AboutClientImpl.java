/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about.client;

import java.util.Map;

import org.alljoyn.about.AboutKeys;
import org.alljoyn.about.transport.AboutTransport;
import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.ProxyBusObject;
import org.alljoyn.bus.Variant;
import org.alljoyn.services.common.BusObjectDescription;
import org.alljoyn.services.common.ClientBaseImpl;
import org.alljoyn.services.common.ServiceAvailabilityListener;
import org.alljoyn.services.common.utils.TransportUtil;

public class AboutClientImpl extends ClientBaseImpl implements AboutClient
{
	public final static String TAG = AboutClientImpl.class.getName();
	
	public AboutClientImpl(String deviceName, BusAttachment bus, ServiceAvailabilityListener serviceAvailabilityListener, short port)
	{
		super(deviceName, bus, serviceAvailabilityListener,AboutTransport.OBJ_PATH, AboutTransport.class, port);
	}

	@Override
	public String[] getLanguages() throws BusException
	{
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		AboutTransport aboutTransport =  proxyObj.getInterface(AboutTransport.class);
		Map<String, Variant> aboutMap = aboutTransport.GetAboutData("en");
		Map<String, Object> fromVariantMap = TransportUtil.fromVariantMap(aboutMap);
		String[] languages = (String[]) fromVariantMap.get(AboutKeys.ABOUT_SUPPORTED_LANGUAGES);
		if (languages != null && languages.length > 0)
		{
			return languages;
		}
		else
		{
			String defaultLanaguage = (String) fromVariantMap.get(AboutKeys.ABOUT_DEFAULT_LANGUAGE);
			return new String[]{defaultLanaguage};
		}
	}

	@Override
	public String getDefaultLanguage() throws BusException
	{
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		AboutTransport aboutTransport =  proxyObj.getInterface(AboutTransport.class);
		Map<String, Variant> aboutMap = aboutTransport.GetAboutData("en");
		Map<String, Object> fromVariantMap = TransportUtil.fromVariantMap(aboutMap);
		String defaultLanaguage = (String) fromVariantMap.get(AboutKeys.ABOUT_DEFAULT_LANGUAGE);
		return defaultLanaguage;
	}

	@Override
	public Map<String, Object> getAbout(String languageTag) throws BusException
	{
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		AboutTransport aboutTransport =  proxyObj.getInterface(AboutTransport.class);
		Map<String, Variant> aboutMap = aboutTransport.GetAboutData(languageTag);
		return TransportUtil.fromVariantMap(aboutMap);
	}
	
	@Override
	public BusObjectDescription[] getBusObjectDescriptions() throws BusException
	{
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		AboutTransport aboutTransport =  proxyObj.getInterface(AboutTransport.class);
		BusObjectDescription[] busObjectDescriptions = aboutTransport.GetObjectDescription();
		return busObjectDescriptions;
	}

}
