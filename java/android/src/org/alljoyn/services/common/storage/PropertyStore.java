/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common.storage;

import java.util.Map;


public interface PropertyStore {
	
	enum Filter {
		ANNOUNCE,//!< ANNOUNCE Property that has  ANNOUNCE  enabled
		READ,    //!< READ     Property that has READ  enabled -- About
		WRITE,   //!< WRITE    Property that has  WRITE  enabled -- Config
	}

	void resetConfiguration(String languageTag, String[] fieldList);

	Map<String, Object>  ReadAll(String languageTag, Filter filter);
	
	void updateConfiguration(Map<String,Object> newConfiguration, String languageTag) throws IllegalAccessException;

	void resetToFactoryDefault();

	void setPassphrase(String passphrase);

	String getPassphrase();
	
	public void setValue(String key, Object value, String languageTag);

}
