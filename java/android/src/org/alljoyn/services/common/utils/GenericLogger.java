/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common.utils;

public interface GenericLogger {
	/**
	 * Debug level message
	 * @param TAG Tag to be added to the message, i.e. class that writes the message 
	 * @param msg 
	 */
	public void debug(String TAG, String msg);
	
	/**
	 * Info level message
	 * @param TAG Tag to be added to the message, i.e. class that writes the message 
	 * @param msg 
	 */
	public void info(String TAG, String msg);

	/**
	 * Warn level message
	 * @param TAG Tag to be added to the message, i.e. class that writes the message 
	 * @param msg 
	 */
	public void warn(String TAG, String msg);

	/**
	 * Error level message
	 * @param TAG Tag to be added to the message, i.e. class that writes the message 
	 * @param msg 
	 */
	public void error(String TAG, String msg);
	
	/**
	 * Fatal level message
	 * @param TAG Tag to be added to the message, i.e. class that writes the message 
	 * @param msg 
	 */
	public void fatal(String TAG, String msg);
}
