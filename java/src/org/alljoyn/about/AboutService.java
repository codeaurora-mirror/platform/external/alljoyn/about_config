/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about;

import java.util.List;

import org.alljoyn.about.client.AboutClient;
import org.alljoyn.about.icon.AboutIconClient;
import org.alljoyn.about.transport.AboutTransport;
import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.services.common.AnnouncementHandler;
import org.alljoyn.services.common.BusObjectDescription;
import org.alljoyn.services.common.ServiceAvailabilityListener;
import org.alljoyn.services.common.ServiceCommon;
import org.alljoyn.services.common.storage.PropertyStore;

public interface AboutService extends ServiceCommon 
{
	
	/** Client */
	public static final int PROTOCOL_VERSION = 1;
	public static final String ANNOUNCE_IFNAME   = AboutTransport.INTERFACE_NAME;
	public static final String SIGNAL_NAME       = "Announce";

	public void startAboutClient(/*String appName, */BusAttachment bus/*, AnnouncementHandler receiver*/) throws Exception; 
	public AboutClient createAboutClient(String peerName, ServiceAvailabilityListener serviceAvailabilityListener, short port) throws Exception;
	public void stopAboutClient() throws Exception;
	public void addAnnouncementHandler(AnnouncementHandler handler);
	public void removeAnnouncementHandler(AnnouncementHandler handler);
	
	/** server */
	public void startAboutServer(short port, /*String appName, */PropertyStore propertyStore, BusAttachment m_bus) throws Exception;
	public void stopAboutServer() throws Exception;
//	Announcer getAnnouncer();

	public void addObjectDescription(String objPath, String [] interfaces);
	public void removeObjectDescription(String objPath, String [] interfaces);
	public void addObjectDescriptions(List<BusObjectDescription> addBusObjectDescriptions);
	public void removeObjectDescriptions(List<BusObjectDescription> removeBusObjectDescriptions);
	public void announce();

	//Icon
	public AboutIconClient createAboutIconClient(String peerName, ServiceAvailabilityListener serviceAvailabilityListener, short port) throws BusException;
	
}
