/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about;

public interface AboutKeys 
{
	/**
	 * About Keys
	 */

	public final static String ABOUT_DEVICE_ID = "DeviceId"; // Required. The device globally unique id. It is populated with GUID of the App bundling Config service (System App). GUID is generated based on RFC 4122 and persisted on the device.
	public final static String ABOUT_DEVICE_NAME = "DeviceName"; // Required. It is the device name as assigned by the user. The name will be shown on the UI as friendly 
	public final static String ABOUT_APP_ID = "AppId"; // Required. The app globally unique id
	public final static String ABOUT_APP_NAME = "AppName"; // Required. This field specifies the Application Name. It is assigned by the App Manufacturer
	public final static String ABOUT_PASSPHRASE = "Passphrase"; // Required. The device/app globally unique id
	public final static String ABOUT_MANUFACTURER = "Manufacturer";	// Required. The manufacturer�s name of the device/app
	public final static String ABOUT_DESCRIPTION = "Description"; // Required. Detailed description
	public final static String ABOUT_DEFAULT_LANGUAGE = "DefaultLanguage"; // Required. default language supported by the device. IETF language tags specified by  RFC 5646.
	public final static String ABOUT_SOFTWARE_VERSION = "SoftwareVersion"; // Required. The software version
	public final static String ABOUT_AJ_SOFTWARE_VERSION = "AJSoftwareVersion"; // Required. 
	public final static String ABOUT_MODEL_NUMBER = "ModelNumber"; // Not required. The device/app model number
	public final static String ABOUT_DATE_OF_MANUFACTURE = "DateOfManufacture"; // Not required. The date of manufacture using format MM/DD/YYYY
	public final static String ABOUT_HARDWARE_VERSION = "HardwareVersion"; // Not required. The device hardware version
	public final static String ABOUT_SUPPORT_URL = "SupportURL"; // The support URL to be populated by device OEM
	public final static String ABOUT_OEM_REVERSED_DOMAIN_NAME = "OEMReversedDomainName"; // Used for creating the unique advertised name
	public final static String ABOUT_SUPPORTED_LANGUAGES = "SupportedLanguages"; // Not required. This field returns the list of supported languages by the device. 
	
}
