/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about.icon;

import org.alljoyn.about.transport.IconTransport;
import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.ProxyBusObject;
import org.alljoyn.services.common.ClientBaseImpl;
import org.alljoyn.services.common.ServiceAvailabilityListener;

public class AboutIconClientImpl extends ClientBaseImpl implements AboutIconClient
{
	public final static String TAG = AboutIconClientImpl.class.getName();
	
	public AboutIconClientImpl(String peerName, BusAttachment bus, ServiceAvailabilityListener serviceAvailabilityListener, short port)
	{
		super(peerName, bus, serviceAvailabilityListener, IconTransport.OBJ_PATH, IconTransport.class, port);
	}
	
	@Override
	public short getVersion() throws BusException {
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		IconTransport aboutIconTransport =  proxyObj.getInterface(IconTransport.class);
		return aboutIconTransport.getVersion();
	}

	@Override
	public String getMimeType() throws BusException {
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		IconTransport aboutIconTransport =  proxyObj.getInterface(IconTransport.class);		
		return aboutIconTransport.getMimeType();
	}

	@Override
	public int getSize() throws BusException {
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		IconTransport aboutIconTransport =  proxyObj.getInterface(IconTransport.class);
		return aboutIconTransport.getSize();
	}

	@Override
	public String GetUrl() throws BusException {
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		IconTransport aboutIconTransport =  proxyObj.getInterface(IconTransport.class);
		return aboutIconTransport.GetUrl();
	}

	@Override
	public byte[] GetContent() throws BusException {
		ProxyBusObject proxyObj = getProxyObject();
		/* We make calls to the methods of the AllJoyn object through one of its interfaces. */
		IconTransport aboutIconTransport =  proxyObj.getInterface(IconTransport.class);
		return aboutIconTransport.GetContent();
	}

	
}
