/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.about.transport;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.BusObject;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusMethod;
import org.alljoyn.bus.annotation.BusProperty;

@BusInterface (name = IconTransport.INTERFACE_NAME)
public interface IconTransport extends BusObject
{
	public static final String INTERFACE_NAME = "org.alljoyn.Icon";
	public final static String OBJ_PATH = "/About/DeviceIcon";

	/**
	 * @return Interface version
	 * @throws BusException
	 */
    @BusProperty(signature="q")
    public short getVersion() throws BusException;

	/**
	 * @return Mime type for the icon
	 * @throws BusException
	 */
    @BusProperty(signature="s")
    public String getMimeType() throws BusException;

	/**
	 * @return Size of the icon
	 * @throws BusException
	 */
    @BusProperty(signature="u")
    public int getSize() throws BusException;

    /**
     * Returns the URL if the icon is hosted on the cloud
     * @return the URL if the icon is hosted on the cloud
     * @throws BusException
     */
    @BusMethod(replySignature="s")
    public String GetUrl() throws BusException;

    /**
     * Returns binary content for the icon
     * @return binary content for the icon
     * @throws BusException
     */
    @BusMethod(replySignature="ay")
    public byte[] GetContent() throws BusException;


}
