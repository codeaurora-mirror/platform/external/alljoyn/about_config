/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

package org.alljoyn.services.common;

import java.util.Map;


/**
 * An interface to be implemented in order to receive notification messages
 */
public interface AnnouncementHandler
{
	public void onAnnouncement(String serviceName, short port, BusObjectDescription[] objectDescriptions, Map<String,Object> serviceMetadata);
	public void onDeviceLost(String serviceName);
}
