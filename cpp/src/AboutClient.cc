/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#include <stdio.h>
#include "alljoyn/about/AboutClient.h"
#include <qcc/Debug.h>


using namespace ajn;
using namespace services;

#define QCC_MODULE "ALLJOYN_ABOUT_CLIENT"


AboutClient::AboutClient(ajn::BusAttachment& bus) :
    announceListener(NULL),
    announceSignalMember(NULL),
    responderThread(NULL),
    stopping(false) {
    QCC_DbgTrace(("AboutClient::%s", __FUNCTION__));
    myBusAttachment = &bus;
    QStatus status = ER_OK;
    const InterfaceDescription* getIface = NULL;
    getIface = myBusAttachment->GetInterface("org.alljoyn.About");
    if (!getIface) {
        InterfaceDescription* createIface = NULL;
        status = myBusAttachment->CreateInterface("org.alljoyn.About",
                                                  createIface, false);
        status = createIface->AddMethod("GetAboutData", "s", "a{sv}",
                                        "languageTag,aboutData");
        status = createIface->AddMethod("GetObjectDescription", NULL, "a(oas)",
                                        "Control");
        status = createIface->AddProperty("Version", "q", PROP_ACCESS_READ);
        status = createIface->AddSignal("Announce", "qqa(oas)a{sv}",
                                        "version,port,objectDescription,servMetadata", 0);
        createIface->Activate();
        announceSignalMember = createIface->GetMember("Announce");
    } else {
        announceSignalMember = getIface->GetMember("Announce");
    }

}

void* AboutClient::ResponderThreadWrapper(void* arg) {
    AboutClient* cr = reinterpret_cast<AboutClient*>(arg);
    assert(cr);
    cr->ResponderThread();
    return NULL;
}

void AboutClient::ResponderThread() {
    pthread_mutex_lock(&lock);
    while (!stopping) {
        while (!messagesQueue.empty()) {
            std::pair<ajn::Message, time_t>  dataPair = messagesQueue.front();
            Message message = dataPair.first;
            messagesQueue.pop();
            pthread_mutex_unlock(&lock);
            if (announceListener) {
                const ajn::MsgArg* args;
                size_t numArgs;
                QStatus status;
                message->GetArgs(numArgs, args);
                if (numArgs == 4) {
#if !defined(NDEBUG)
                    for (int i = 0; i < 4; i++) {
                        QCC_DbgPrintf(("args[%d]=%s", i, args[i].ToString().c_str()));
                    }
#endif
                    int version;
                    int receviedPort;
                    AboutData aboutData;
                    ObjectDescriptions objectDescriptions;
                    status = args[0].Get("q", &version);
                    if (status != ER_OK) {
                        return;
                    }
                    status = args[1].Get("q", &receviedPort);
                    if (status != ER_OK) {
                        return;
                    }
                    MsgArg*objectDescriptionsArgs;
                    size_t objectNum;
                    status = args[2].Get("a(oas)", &objectNum, &objectDescriptionsArgs);
                    if (status != ER_OK) {
                        return;
                    }
                    for (size_t i = 0; i < objectNum; i++) {
                        char* objectDescriptionPath;
                        MsgArg*interfaceEntries;
                        size_t interfaceNum;
                        status = objectDescriptionsArgs[i].Get("(oas)", &objectDescriptionPath, &interfaceNum, &interfaceEntries);
                        if (status != ER_OK) {
                            return;
                        }
                        std::vector<qcc::String> localVector;
                        for (size_t i = 0; i < interfaceNum; i++) {
                            char*interfaceName;
                            status = interfaceEntries[i].Get("s", &interfaceName);
                            if (status != ER_OK) {
                                return;
                            }
                            localVector.push_back(interfaceName);
                        }
                        objectDescriptions.insert(std::pair<qcc::String, std::vector<qcc::String> >(objectDescriptionPath, localVector));
                    }
                    MsgArg*tempControlArg2;
                    int languageTagNumElements;
                    status = args[3].Get("a{sv}", &languageTagNumElements, &tempControlArg2);
                    for (int i = 0; i < languageTagNumElements; i++) {
                        char*tempKey;
                        MsgArg* tempValue;
                        status = tempControlArg2[i].Get("{sv}", &tempKey, &tempValue);
                        aboutData.insert(std::pair<qcc::String, ajn::MsgArg>(tempKey, *tempValue));
                    }
                    args[1].Get("q", &receviedPort);
                    announceListener->Announce(version, receviedPort, message->GetSender(), objectDescriptions, aboutData);
                }
            }
            pthread_mutex_lock(&lock);
        }
        pthread_cond_wait(&queueChanged, &lock);
    }
    pthread_mutex_unlock(&lock);
}




AboutClient::~AboutClient() {
    if (announceListener) {
        pthread_mutex_lock(&lock);
        while (!messagesQueue.empty()) {
            messagesQueue.pop();
        }
        stopping = true;
        pthread_cond_signal(&queueChanged);
        pthread_mutex_unlock(&lock);
        pthread_join(responderThread, NULL);
        myBusAttachment->UnregisterSignalHandler(this, SignalHandler(&AboutClient::AnnounceHandler), announceSignalMember, NULL);
        pthread_cond_destroy(&queueChanged);
        pthread_mutex_destroy(&lock);
    }
}






QStatus AboutClient::GetObjectDescriptions(const char* busName, AboutClient::ObjectDescriptions& objectDescs, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface("org.alljoyn.About");
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    status = proxyBusObj->AddInterface(*ifc);
    if (status != ER_OK) {
        delete proxyBusObj;
        return status;
    }
    Message replyMsg(*myBusAttachment);
    MsgArg args[0];
    status = proxyBusObj->MethodCall("org.alljoyn.About", "GetObjectDescription", args, 0, replyMsg);
    if (status != ER_OK) {
        delete proxyBusObj;
        proxyBusObj = NULL;
        return status;
    }
    const ajn::MsgArg* returnArgs;
    size_t numArgs;
    replyMsg->GetArgs(numArgs, returnArgs);
    if (numArgs == 1) {
        MsgArg*objectDescriptionsArgs;
        size_t objectNum;
        status = returnArgs[0].Get("a(oas)", &objectNum, &objectDescriptionsArgs);
        if (status != ER_OK) {
            return status;
        }
        for (size_t i = 0; i < objectNum; i++) {
            char* objectDescriptionPath;
            MsgArg*interfaceEntries;
            size_t interfaceNum;
            status = objectDescriptionsArgs[i].Get("(oas)", &objectDescriptionPath, &interfaceNum, &interfaceEntries);
            if (status != ER_OK) {
                return status;
            }
            std::vector<qcc::String> localVector;
            for (size_t i = 0; i < interfaceNum; i++) {
                char*interfaceName;
                status = interfaceEntries[i].Get("s", &interfaceName);
                if (status != ER_OK) {
                    return status;
                }
                localVector.push_back(interfaceName);
            }
            objectDescs.insert(std::pair<qcc::String, std::vector<qcc::String> >(objectDescriptionPath, localVector));
        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;
    return status;
}

QStatus AboutClient::GetAboutData(const char* busName, const char* languageTag, AboutClient::AboutData& data, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface("org.alljoyn.About");
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    status = proxyBusObj->AddInterface(*ifc);
    if (status != ER_OK) {
        delete proxyBusObj;
        return status;
    }
    Message replyMsg(*myBusAttachment);
    MsgArg args[1];
    args[0].Set("s", languageTag);
    status = proxyBusObj->MethodCall("org.alljoyn.About", "GetAboutData", args, 1, replyMsg);
    if (status != ER_OK) {
        delete proxyBusObj;
        proxyBusObj = NULL;
        if (status == ER_BUS_REPLY_IS_ERROR_MESSAGE) {
            qcc::String errorMessage;
            const char* errorName = replyMsg->GetErrorName(&errorMessage);
            QCC_LogError(status, ("GetAboutData ::Error name=%s ErorrMessage=%s", errorName, errorMessage.c_str()));
        }
        return status;
    }
    const ajn::MsgArg* returnArgs;
    size_t numArgs;
    replyMsg->GetArgs(numArgs, returnArgs);
    if (numArgs == 1) {
        size_t languageTagNumElements;
        MsgArg*tempControlArg;
        status = returnArgs[0].Get("a{sv}", &languageTagNumElements, &tempControlArg);
        for (unsigned int i = 0; i < languageTagNumElements; i++) {
            char*tempKey;
            MsgArg* tempValue;
            status = tempControlArg[i].Get("{sv}", &tempKey, &tempValue);
            data.insert(std::pair<qcc::String, ajn::MsgArg>(tempKey, *tempValue));
        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;
    return status;
}

QStatus AboutClient::GetVersion(const char* busName, int& version, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface("org.alljoyn.About");
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    MsgArg arg;
    if (ER_OK == proxyBusObj->IntrospectRemoteObject()) {
        status = proxyBusObj->GetProperty("org.alljoyn.About", "Version", arg);
        if (ER_OK == status) {
            version = arg.v_variant.val->v_int16;

        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;
    return status;
}

void AboutClient::AnnounceHandler(const ajn::InterfaceDescription::Member* member, const char* srcPath, ajn::Message& msg) {
    QCC_DbgTrace(("AboutClient::%s", __FUNCTION__));
    pthread_mutex_lock(&lock);
    messagesQueue.push(std::pair<ajn::Message, time_t> (msg, time(0)));
    pthread_cond_signal(&queueChanged);
    pthread_mutex_unlock(&lock);
}

void AboutClient::RegisterAnnounceListener(AnnounceListener& listener) {
    QCC_DbgTrace(("AboutClient::%s", __FUNCTION__));
    announceListener = &listener;
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&queueChanged, NULL);
    const InterfaceDescription* ifc = myBusAttachment->GetInterface("org.alljoyn.About");
    assert(ifc);
    announceSignalMember = ifc->GetMember("Announce");
    myBusAttachment->RegisterSignalHandler(this, SignalHandler(&AboutClient::AnnounceHandler), announceSignalMember, NULL);
    pthread_create(&responderThread, NULL, ResponderThreadWrapper, this);
    myBusAttachment->AddMatch("sessionless='t',interface='org.alljoyn.About'");
}


