/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#include <alljoyn/about/AboutIconService.h>

#include <alljoyn/BusAttachment.h>
#include <alljoyn/BusObject.h>
#include <stdint.h>

#include <qcc/Debug.h>
#define QCC_MODULE "ALLJOYN_ABOUT_ICON_SERVICE"

using namespace ajn;
using namespace services;

static const char* about_icon_interface_name = "org.alljoyn.Icon";

AboutIconService::AboutIconService(ajn::BusAttachment& bus,
                                   qcc::String mimetype,
                                   qcc::String url,
                                   uint8_t* content,
                                   size_t contentSize) :
    BusObject("/About/DeviceIcon"),
    m_mimeType(mimetype),
    m_url(url),
    m_content(content),
    m_contentSize(contentSize) {
    QCC_DbgTrace(("AboutIconService::%s", __FUNCTION__));
    myBusAttachment = &bus;
}

QStatus AboutIconService::Register() {
    QStatus status = ER_OK;
    QCC_DbgTrace(("AboutIconService::%s", __FUNCTION__));

    const InterfaceDescription* getIface = NULL;
    getIface = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!getIface) {
        InterfaceDescription* createIface = NULL;
        status = myBusAttachment->CreateInterface(about_icon_interface_name, createIface, false);
        status = createIface->AddMethod("GetUrl", NULL, "s", "url");
        status = createIface->AddMethod("GetContent", NULL, "ay", "content");
        status = createIface->AddProperty("Version", "q", (uint8_t) PROP_ACCESS_READ);
        status = createIface->AddProperty("MimeType", "s", (uint8_t) PROP_ACCESS_READ);
        status = createIface->AddProperty("Size", "u", (uint8_t) PROP_ACCESS_READ);
        createIface->Activate();
        status = AddInterface(*createIface);
        status = AddMethodHandler(createIface->GetMember("GetUrl"), static_cast<MessageReceiver::MethodHandler>(&AboutIconService::GetUrl));
        status = AddMethodHandler(createIface->GetMember("GetContent"), static_cast<MessageReceiver::MethodHandler>(&AboutIconService::GetContent));
    } else {
        status = AddInterface(*getIface);
        status = AddMethodHandler(getIface->GetMember("GetUrl"), static_cast<MessageReceiver::MethodHandler>(&AboutIconService::GetUrl));
        status = AddMethodHandler(getIface->GetMember("GetContent"), static_cast<MessageReceiver::MethodHandler>(&AboutIconService::GetContent));
    }

    return status;
}

void AboutIconService::GetUrl(const ajn::InterfaceDescription::Member* member, ajn::Message& msg) {
    QCC_DbgTrace(("AboutIconService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const ajn::MsgArg* args;
    size_t numArgs;
    msg->GetArgs(numArgs, args);
    if (numArgs == 0) {
        ajn::MsgArg retargs[1];
        status = retargs[0].Set("s", m_url.c_str());
        status = MethodReply(msg, retargs, 1);
    } else {
        MethodReply(msg, ER_INVALID_DATA);
    }

}

void AboutIconService::GetContent(const ajn::InterfaceDescription::Member* member, ajn::Message& msg) {
    QCC_DbgTrace(("AboutIconService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const ajn::MsgArg* args;
    size_t numArgs;
    msg->GetArgs(numArgs, args);
    if (numArgs == 0) {
        ajn::MsgArg retargs[1];
        status = retargs[0].Set("ay", m_contentSize, m_content);
        status = MethodReply(msg, retargs, 1);
    } else {
        MethodReply(msg, ER_INVALID_DATA);
    }
}

QStatus AboutIconService::Get(const char*ifcName, const char*propName, MsgArg& val) {
    QCC_DbgTrace(("AboutIconService::%s", __FUNCTION__));
    QStatus status = ER_OK;
    if (0 == strcmp(ifcName, about_icon_interface_name)) {
        if (0 == strcmp("Version", propName)) {
            val.Set("q", 1);
        } else if (0 == strcmp("MimeType", propName)) {
            val.Set("s", m_mimeType.c_str());
        } else if (0 == strcmp("Size", propName)) {
            val.Set("u", m_contentSize);
        }
    } else {
        status = ER_BUS_NO_SUCH_PROPERTY;
    }
    return status;
}
