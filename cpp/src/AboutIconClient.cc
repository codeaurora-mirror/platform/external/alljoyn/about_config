/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#include "alljoyn/about/AboutIconClient.h"
#include <qcc/Debug.h>
#define QCC_MODULE "ALLJOYN_ABOUT_ICON_CLIENT"

using namespace ajn;
using namespace services;

static const char* about_icon_interface_name = "org.alljoyn.Icon";

AboutIcontClient::AboutIcontClient(ajn::BusAttachment& bus) {

    QCC_DbgTrace(("AboutIcontClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    myBusAttachment = &bus;
    const InterfaceDescription* getIface = NULL;
    getIface = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!getIface) {
        InterfaceDescription* createIface = NULL;
        status = myBusAttachment->CreateInterface(about_icon_interface_name, createIface, false);
        status = createIface->AddMethod("GetUrl", NULL, "s", "url");
        status = createIface->AddMethod("GetContent", NULL, "ay", "content");
        status = createIface->AddProperty("Version", "q", (uint8_t) PROP_ACCESS_READ);
        status = createIface->AddProperty("MimeType", "s", (uint8_t) PROP_ACCESS_READ);
        status = createIface->AddProperty("Size", "u", (uint8_t) PROP_ACCESS_READ);
        createIface->Activate();
    }
}

QStatus AboutIcontClient::GetUrl(const char* busName, qcc::String& url, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutIcontClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About/DeviceIcon", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    status = proxyBusObj->AddInterface(*ifc);
    if (status != ER_OK) {
        delete proxyBusObj;
        return status;
    }
    Message replyMsg(*myBusAttachment);
    status = proxyBusObj->MethodCall(about_icon_interface_name, "GetUrl", NULL, 0, replyMsg);
    if (status == ER_OK) {
        const ajn::MsgArg* returnArgs;
        size_t numArgs;
        replyMsg->GetArgs(numArgs, returnArgs);
        if (numArgs == 1) {
            char* temp;
            status = returnArgs[0].Get("s", &temp);
            if (status == ER_OK) {
                url.assign(temp);
            }

        } else {
            status = ER_BUS_BAD_VALUE;
        }
    }
    delete proxyBusObj;
    return status;
}

QStatus AboutIcontClient::GetContent(const char* busName, uint8_t** content, size_t& contentSize, ajn::SessionId sessionId) {

    QCC_DbgTrace(("AboutIcontClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About/DeviceIcon", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    status = proxyBusObj->AddInterface(*ifc);
    if (status != ER_OK) {
        delete proxyBusObj;
        proxyBusObj = NULL;
        return status;
    }
    Message replyMsg(*myBusAttachment);
    status = proxyBusObj->MethodCall(about_icon_interface_name, "GetContent", NULL, 0, replyMsg);
    if (status == ER_OK) {
        const ajn::MsgArg* returnArgs;
        size_t numArgs;
        replyMsg->GetArgs(numArgs, returnArgs);
        if (numArgs == 1) {

            status = returnArgs[0].Get("ay", &contentSize, content);
        } else {
            status = ER_BUS_BAD_VALUE;
        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;
    return status;

}

QStatus AboutIcontClient::GetVersion(const char* busName, int& version, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutIcontClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About/DeviceIcon", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    MsgArg arg;
    if (ER_OK == proxyBusObj->IntrospectRemoteObject()) {
        status = proxyBusObj->GetProperty(about_icon_interface_name, "Version", arg);
        if (ER_OK == status) {
            version = arg.v_variant.val->v_int16;
        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;
    return status;
}

QStatus AboutIcontClient::GetSize(const char* busName, size_t& size, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutIcontClient::%s", __FUNCTION__));
    QStatus status = ER_OK;
    const InterfaceDescription* ifc = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About/DeviceIcon", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    MsgArg arg;
    if (ER_OK == proxyBusObj->IntrospectRemoteObject()) {
        status = proxyBusObj->GetProperty(about_icon_interface_name, "Size", arg);
        if (ER_OK == status) {
            size = arg.v_variant.val->v_uint64;
        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;
    return status;
}

QStatus AboutIcontClient::GetMimeType(const char* busName, qcc::String& mimeType, ajn::SessionId sessionId) {
    QCC_DbgTrace(("AboutIcontClient::%s", __FUNCTION__));
    QStatus status = ER_OK;

    const InterfaceDescription* ifc = myBusAttachment->GetInterface(about_icon_interface_name);
    if (!ifc) {
        return ER_FAIL;
    }
    ProxyBusObject*proxyBusObj = new ProxyBusObject(*myBusAttachment, busName, "/About/DeviceIcon", sessionId);
    if (!proxyBusObj) {
        return ER_FAIL;
    }
    MsgArg arg;
    if (ER_OK == proxyBusObj->IntrospectRemoteObject()) {
        status = proxyBusObj->GetProperty(about_icon_interface_name, "MimeType", arg);
        if (ER_OK == status) {
            char* temp;
            arg.Get("s", &temp);
            mimeType.assign(temp);
        }
    }
    delete proxyBusObj;
    proxyBusObj = NULL;

    return status;
}

