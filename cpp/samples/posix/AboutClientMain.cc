/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#include <qcc/platform.h>
#include <stdio.h>
#include <qcc/String.h>
#include <signal.h>
#include <alljoyn/version.h>

#include <alljoyn/BusObject.h>
#include <alljoyn/BusAttachment.h>

#include <alljoyn/Status.h>


#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/tree.h>
#include <fstream>

#include <alljoyn/about/AboutClient.h>
#include <alljoyn/about/AboutIconClient.h>
#include "qcc/Log.h"
#include "stdint.h"




using namespace ajn;
using namespace std;
using namespace services;

static volatile sig_atomic_t quit;
static BusAttachment*busAttachment;
static void SignalHandler(int sig) {
    switch (sig) {
    case SIGINT:
    case SIGTERM:
        quit = 1;
        break;
    }
}


#define SERVICE_EXIT_OK       0
#define SERVICE_OPTION_ERROR  1
#define SERVICE_CONFIG_ERROR  2




class AboutClientSessionListener : public ajn::SessionListener {
  public:
    AboutClientSessionListener(qcc::String inServiceNAme) :
        serviceName(inServiceNAme) {
        mySessionID = 0;
    }
    void SessionLost(ajn::SessionId sessionId) {
        printf("AboutClient %s session has been lost\n", serviceName.c_str());
    }
    ajn::SessionId mySessionID;
  private:
    qcc::String serviceName;

};

class JoinCB : public BusAttachment::JoinSessionAsyncCB {
  public:
    qcc::String busname;
    const SessionPort port;

    JoinCB(const char* name, SessionPort port) :
        port(port) {
        busname.assign(name);
    }

    void JoinSessionCB(QStatus status, SessionId id, const SessionOpts& opts, void* context) {

        if (status == ER_OK) {

            busAttachment->EnableConcurrentCallbacks();
            AboutClient* aboutClient = new AboutClient(*busAttachment);


            AboutIcontClient*iconClient = NULL;
            bool isIconInterface = false;
            printf("\n%s AboutClient ObjectDescriptions\n-----------------------------------\n", busname.c_str());
            AboutClient::ObjectDescriptions ObjectDescriptionsRefill;
            status = aboutClient->GetObjectDescriptions(busname.c_str(), ObjectDescriptionsRefill, id);

            if (status != ER_OK) {
                printf("Unable get  ObjectDescriptions \n");
            } else {
                for (AboutClient::ObjectDescriptions::const_iterator it = ObjectDescriptionsRefill.begin(); it != ObjectDescriptionsRefill.end(); ++it) {
                    qcc::String key = it->first;
                    std::vector<qcc::String> vector = it->second;
                    printf("key=%s", key.c_str());
                    for (std::vector<qcc::String>::const_iterator itv = vector.begin(); itv != vector.end(); ++itv) {
                        if (key.compare("/About/DeviceIcon") == 0 && itv->compare("org.alljoyn.Icon") == 0) {
                            isIconInterface = true;
                        }
                        if (vector.size() == 1) {
                            printf(" value=%s\n", itv->c_str());
                        } else {
                            printf(" value=%s", itv->c_str());
                        }
                    }
                    if (vector.size() > 1) {
                        printf("\n");
                    }
                }
            }

            printf("\n %s AboutClient AboutData\n-----------------------------------\n", busname.c_str());
            AboutClient::AboutData aboutDataRefill;
            status = aboutClient->GetAboutData(busname.c_str(), "en", aboutDataRefill);
            if (status != ER_OK) {
                printf("Unable get  AboutData \n");
            } else {
                for (AboutClient::AboutData::iterator itx = aboutDataRefill.begin(); itx != aboutDataRefill.end(); ++itx) {
                    qcc::String key = itx->first;
                    ajn::MsgArg value = itx->second;
                    if (value.typeId == ALLJOYN_STRING) {
                        printf("Key name=%s value=%s\n", key.c_str(), value.v_string.str);
                    } else if (value.typeId == ALLJOYN_ARRAY && value.Signature().compare("as") == 0) {
                        printf("Key name=%s values: ", key.c_str());
                        const MsgArg*stringArray;
                        size_t fieldListNumElements;
                        status = value.Get("as", &fieldListNumElements, &stringArray);
                        for (unsigned int i = 0; i < fieldListNumElements; i++) {
                            char* tempString;
                            stringArray[i].Get("s", &tempString);
                            printf("%s ", tempString);
                        }
                        printf("\n");
                    } else if (value.typeId == ALLJOYN_BYTE_ARRAY) {
                        printf("Key name=%s value:", key.c_str());
                        uint8_t*AppIdBuffer;
                        size_t numElements;
                        value.Get("ay", &numElements, &AppIdBuffer);
                        for (size_t i = 0; i < numElements; i++) {
                            printf("%02X", AppIdBuffer[i]);
                        }
                        printf("\n");
                    }
                }
            }
            printf("\n %s AboutClient GetVersion\n-----------------------------------\n", busname.c_str());
            int ver;
            status = aboutClient->GetVersion(busname.c_str(), ver, id);
            if (status != ER_OK) {
                printf("Unable get  Version \n");
            } else {
                printf("Version=%d \n", ver);
            }

            if (isIconInterface) {
                iconClient = new AboutIcontClient(*busAttachment);

                size_t contentSize;

                printf("\n%s AboutIcontClient GetUrl \n-----------------------------------\n", busname.c_str());
                qcc::String url;
                status = iconClient->GetUrl(busname.c_str(), url, id);
                if (status != ER_OK) {
                    printf("Unable get  url \n");
                } else {
                    printf("url=%s\n", url.c_str());
                }

                printf("\n%s AboutIcontClient GetContent \n-----------------------------------\n", busname.c_str());

                uint8_t*content = NULL;
                status = iconClient->GetContent(busname.c_str(), &content, contentSize, id);

                if (status != ER_OK) {
                    printf("Unable get  GetContent \n");
                } else {
                    printf("Content size=%lu\n", contentSize);
                    printf("Content :\t");
                    for (size_t i = 0; i < contentSize; i++) {
                        if (i % 8 == 0 && i > 0)
                            printf("\n\t\t");
                        printf("%02X", content[i]);
                    }
                    printf("\n");
                }

                printf("\n%s AboutIcontClient GetVersion \n-----------------------------------\n", busname.c_str());

                status = iconClient->GetVersion(busname.c_str(), ver, id);
                if (status != ER_OK) {
                    printf("Unable get  Version \n");
                } else {
                    printf("Version=%d \n", ver);
                }

                printf("\n%s AboutIcontClient GetMimeType \n-----------------------------------\n", busname.c_str());
                qcc::String mimetype;

                status = iconClient->GetMimeType(busname.c_str(), mimetype, id);
                if (status != ER_OK) {
                    printf("Unable get  Mimetype \n");
                } else {
                    printf("Mimetype %s \n", mimetype.c_str());
                }

                printf("\n%s AboutIcontClient GetSize \n-----------------------------------\n", busname.c_str());

                status = iconClient->GetSize(busname.c_str(), contentSize, id);
                if (status != ER_OK) {
                    printf("Unable get  Size \n");
                } else {
                    printf("Size=%lu\n", contentSize);
                }
            }

            status = busAttachment->LeaveSession(port);

            printf("JoinSessionCB(%s, %u, ...) succeeded with id = %u\n", busname.c_str(), port, id);

            if (aboutClient) {
                delete aboutClient;
                aboutClient = NULL;
            }

            if (iconClient) {
                delete iconClient;
                iconClient = NULL;
            }

        } else {
            printf("JoinSessionCB(%s, %u, ...) failed with %s\n", busname.c_str(), port, QCC_StatusText(status));
        }

        delete this;
    }
};







class AnnounceListenerImpl : public AboutClient::AnnounceListener {
  public:
    ~AnnounceListenerImpl() {

    }
    void Announce(int version, int port, const char* busName, const AboutClient::ObjectDescriptions& objectDescs, const AboutClient::AboutData& aboutData) {

        printf("\n\n*********************************************************************************\n");
        printf("version  %d\n", version);
        printf("port  %d\n", port);
        printf("busName  %s\n", busName);
        printf("ObjectDescriptions\n");
        for (AboutClient::ObjectDescriptions::const_iterator it = objectDescs.begin(); it != objectDescs.end(); ++it) {
            qcc::String key = it->first;
            std::vector<qcc::String> vector = it->second;
            printf("key=%s", key.c_str());
            for (std::vector<qcc::String>::const_iterator itv = vector.begin(); itv != vector.end(); ++itv) {
                if (vector.size() == 1) {
                    printf(" value=%s\n", itv->c_str());
                } else {
                    printf(" value=%s", itv->c_str());
                }
            }
            if (vector.size() > 1) {
                printf("\n");
            }
        }
        printf("Announcedata\n");
        for (AboutClient::AboutData::const_iterator it = aboutData.begin(); it != aboutData.end(); ++it) {
            qcc::String key = it->first;
            ajn::MsgArg value = it->second;
            if (value.typeId == ALLJOYN_STRING) {
                printf("Key name=%s value=%s\n", key.c_str(), value.v_string.str);
            } else if (value.typeId == ALLJOYN_BYTE_ARRAY) {
                printf("Key name=%s value:", key.c_str());
                uint8_t*AppIdBuffer;
                size_t numElements;
                value.Get("ay", &numElements, &AppIdBuffer);
                for (size_t i = 0; i < numElements; i++) {
                    printf("%X", AppIdBuffer[i]);
                }
                printf("\n");
            }

        }

        printf("*********************************************************************************\n\n");


        SessionOpts opts(SessionOpts::TRAFFIC_MESSAGES, false, SessionOpts::PROXIMITY_ANY, TRANSPORT_ANY);

        //todo:: delete aboutClientSessionListener, joincb in AnnounceListenerImpl destructor.

        AboutClientSessionListener* aboutClientSessionListener = new AboutClientSessionListener(busName);
        JoinCB*joincb = new JoinCB(busName, aboutClientSessionListener->mySessionID);

        QStatus status = busAttachment->JoinSessionAsync(busName, port, aboutClientSessionListener, opts, joincb);

        if (status != ER_OK) {
            printf("Unable to JoinSession with %s  \n", busName);
            return;
        }

    }
};

static const char versionPreamble[] =
    "AboutClient version: %s\n"
    "Copyright (c) 2009-2013 Qualcomm Innovation Center, Inc.\n"
    "Licensed under the 3-clause BSD license: http://opensource.org/licenses/BSD-3-Clause\n";
//"Build: %s\n";


class OptParse {
  public:
    enum ParseResultCode {
        PR_OK,
        PR_EXIT_NO_ERROR,
        PR_OPTION_CONFLICT,
        PR_INVALID_OPTION,
        PR_MISSING_OPTION

    };

    OptParse(int argc, char** argv) : argc(argc), argv(argv), internal(false) {

    }

    ParseResultCode ParseResult();


    qcc::String  GetDaemonSpec() const {
        return daemonSpec;
    }

    bool  GetInternal() const {
        return internal;
    }

  private:
    int argc;
    char** argv;


    bool internal;
    void PrintUsage();

  private:
    qcc::String daemonSpec;
};


void OptParse::PrintUsage() {
    qcc::String cmd = argv[0];
    cmd = cmd.substr(cmd.find_last_of('/') + 1);

    fprintf(
        stderr,
        "%s [--daemonSpec] "
        "]\n"
        "    --daemonspec=\n"
        "       daemon spec used by the service.\n\n"
        "    --version\n"
        "        Print the version and copyright string, and exit.\n",
        cmd.c_str());
}


OptParse::ParseResultCode OptParse::ParseResult()
{
    ParseResultCode result = PR_OK;
    int i;

    if (argc == 1) {
        internal = true;
        goto exit;
    }

    for (i = 1; i < argc; ++i) {
        qcc::String arg(argv[i]);
        if (arg.compare("--version") == 0) {
            printf(versionPreamble, "1");
            result = PR_EXIT_NO_ERROR;
            goto exit;
        } else if (arg.compare(0, sizeof("--daemonspec") - 1, "--daemonspec") == 0) {
            daemonSpec = arg.substr(sizeof("--daemonspec"));
        }  else if ((arg.compare("--help") == 0) || (arg.compare("-h") == 0)) {
            PrintUsage();
            result = PR_EXIT_NO_ERROR;
            goto exit;
        } else {
            result = PR_INVALID_OPTION;
            goto exit;
        }
    }

exit:

    switch (result) {
    case PR_OPTION_CONFLICT:
        fprintf(stderr,
                "Option \"%s\" is in conflict with a previous option.\n",
                argv[i]);
        break;

    case PR_INVALID_OPTION:
        fprintf(stderr, "Invalid option: \"%s\"\n", argv[i]);
        break;

    case PR_MISSING_OPTION:
        fprintf(stderr, "No config file specified.\n");
        PrintUsage();
        break;

    default:
        break;
    }
    return result;
}


int main(int argc, char**argv, char**envArg) {
    QStatus status = ER_OK;
    printf("AllJoyn Library version: %s\n", ajn::GetVersion());
    printf("AllJoyn Library build info: %s\n", ajn::GetBuildInfo());

    QCC_SetLogLevels("ALLJOYN_ABOUT_CLIENT=7");
    QCC_SetLogLevels("ALLJOYN_ABOUT_ICON_CLIENT=7");



    OptParse opts(argc, argv);
    OptParse::ParseResultCode parseCode(opts.ParseResult());
    switch (parseCode) {
    case OptParse::PR_OK:
        break;

    case OptParse::PR_EXIT_NO_ERROR:

        return SERVICE_EXIT_OK;

    default:
        return SERVICE_OPTION_ERROR;
    }
    if (!opts.GetDaemonSpec().empty()) {
        printf("using Daemon config spec %s\n", opts.GetDaemonSpec().c_str());
    }



    // QCC_SetLogLevels("ALLJOYN=7;ALL=1");
    struct sigaction act, oldact;
    sigset_t sigmask, waitmask;

    // Block all signals by default for all threads.
    sigfillset(&sigmask);
    sigdelset(&sigmask, SIGSEGV);
    pthread_sigmask(SIG_BLOCK, &sigmask, NULL);

    // Setup a handler for SIGINT and SIGTERM
    act.sa_handler = SignalHandler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = SA_SIGINFO | SA_RESTART;
    sigaction(SIGINT, &act, &oldact);
    sigaction(SIGTERM, &act, &oldact);

    busAttachment = new BusAttachment("ChatResponder", true);

    status = busAttachment->Start();
    if (status == ER_OK) {
        printf("BusAttachment started.\n");
    } else {
        printf("unable to connect to Start : %s\n", QCC_StatusText(status));
        return 1;
    }

    if (opts.GetDaemonSpec().empty()) {
        status = busAttachment->Connect();
    } else {
        status = busAttachment->Connect(opts.GetDaemonSpec().c_str());
    }

    if (ER_OK == status) {
        printf("Connect to '%s' succeeded.\n", busAttachment->GetConnectSpec().c_str());
    } else {
        printf("Failed to connect to '%s' (%s).\n", busAttachment->GetConnectSpec().c_str(), QCC_StatusText(status));
        return 1;
    }

    AboutClient*aboutClient = new AboutClient(*busAttachment);
    AnnounceListenerImpl*announceListenerImpl = new AnnounceListenerImpl();
    aboutClient->RegisterAnnounceListener(*announceListenerImpl);

    // Setup signals to wait for.
    sigfillset(&waitmask);
    sigdelset(&waitmask, SIGINT);
    sigdelset(&waitmask, SIGTERM);

    quit = 0;

    while (!quit) {
        // Wait for a signal.
        sigsuspend(&waitmask);
    }


    delete aboutClient;
    delete announceListenerImpl;

    busAttachment->Stop();
    delete busAttachment;

} /* main() */
