About service C++ Samples for Linux README
------------------------------------

This directory contains the following About service C++ API samples:
AboutClientMain.cc -
		This will build into the AboutClient executable.
		With this application it will output all About information
		that was Announced from an application using the About service.

AboutServiceMain.cc -
		This will build into the AboutService executable.
		With this application it will Announce a default set of About
		information.
