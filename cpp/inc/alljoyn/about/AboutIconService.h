/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/

#ifndef ABOUTICONSERVICE_H_
#define ABOUTICONSERVICE_H_

#include <alljoyn/BusObject.h>
#include <qcc/String.h>

namespace ajn {
namespace services {

/**
 * AboutIconService is an AllJoyn BusObject that implements the org.alljoyn.Icon standard interface.
 * Applications that provide AllJoyn IoE services to receive info about the Icon of the service.
 */
class AboutIconService : public ajn::BusObject {
  public:

    /**
     *
     * @param[in] bus  BusAttachment instance associated with this AboutService
     * @param[in]  mimetype of the icon
     * @param[in]  url of the icon
     * @param[in]  data	is the content of the icon
     * @param[in]  csize is the size of the content in bytes.
     */
    AboutIconService(ajn::BusAttachment& bus, qcc::String mimetype, qcc::String url, uint8_t* data, size_t csize);
    /**
     *	Desctructor of AboutIconService
     */
    ~AboutIconService() {
    }

    /**
     * Register the AboutIconService  on the AllJoyn bus.
     * @return status.
     */
    QStatus Register();

  private:

    /**
     *	pointer to BusAttachment
     */
    ajn::BusAttachment* myBusAttachment;

    /**
     * Handles  GetUrl method
     * @param[in]  member
     * @param[in]  msg reference of AllJoyn Message
     */
    void GetUrl(const ajn::InterfaceDescription::Member* member, ajn::Message& msg);

    /**
     *	Handles  GetContent method
     * @param[in]  member
     * @param[in]  msg reference of AllJoyn Message
     */
    void GetContent(const ajn::InterfaceDescription::Member* member, ajn::Message& msg);

    /**
     * Handles the GetPropery request
     * @param[in]  ifcName  interface name
     * @param[in]  propName the name of the properly
     * @param[in]  val reference of MsgArg out parameter.
     * @return
     */
    QStatus Get(const char*ifcName, const char*propName, MsgArg& val);

    /**
     *	stores the mime type of the icon.
     */
    qcc::String m_mimeType;

    /**
     *	stores the url of the icon
     */
    qcc::String m_url;

    /**
     *	stores the content of the icon
     */
    uint8_t* m_content;

    /**
     *	stores the size of the icon's content
     */
    size_t m_contentSize;

};

}
}

#endif /* ABOUTICONSERVICE_H_ */
