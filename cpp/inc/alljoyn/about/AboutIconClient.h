/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/
#ifndef ABOUTICONCLIENT_H_
#define ABOUTICONCLIENT_H_

#include <alljoyn/Message.h>
#include <alljoyn/BusListener.h>
#include <alljoyn/BusObject.h>
#include <alljoyn/BusAttachment.h>
#include <alljoyn/MsgArg.h>
#include <qcc/String.h>

namespace ajn {
namespace services {
/**
 * AboutClient enables the user of the class to interact with the remote AboutServiceIcon  instance exposing the following methods:
 *  GetUrl
 *  GetContent
 *  GetVersion
 *  GetMimeType
 *  GetSize
 */

class AboutIcontClient {
  public:

    /**
     * Construct an AboutIcontClient.
     * @param bus reference to BusAttachment
     */
    AboutIcontClient(ajn::BusAttachment& bus);
    /**
     * Destruct AboutIcontClient
     */
    ~AboutIcontClient() {
    }
    /**
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[in] url of the icon
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful
     */
    QStatus GetUrl(const char* busName, qcc::String& url, ajn::SessionId sessionId = 0);

    /**
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[out] content retrieves the icons payload
     * @param[out] contentSize is the size of the content payload
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful
     */
    QStatus GetContent(const char* busName, uint8_t** content, size_t& contentSize, ajn::SessionId sessionId = 0);

    /**
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[out] version of the AboutIcontClient
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful
     */
    QStatus GetVersion(const char* busName, int& version, ajn::SessionId sessionId = 0);

    /**
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[out] mimeType of the icon
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful
     */
    QStatus GetMimeType(const char* busName, qcc::String& mimeType, ajn::SessionId sessionId = 0);

    /**
     *
     * @param[in] busName Unique or well-known name of AllJoyn node to retrieve ObjectDescriptions from.
     * @param[out] size of the icon
     * @param[in] sessionId the session received  after joining AllJoyn session
     * @return ER_OK if successful
     */
    QStatus GetSize(const char* busName, size_t& size, ajn::SessionId sessionId = 0);

  private:
    /**
     * pointer to BusAttachment
     */
    ajn::BusAttachment* myBusAttachment;

};

}
}

#endif /* ABOUTICONCLIENT_H_ */
