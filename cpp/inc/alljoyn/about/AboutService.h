/******************************************************************************
 * Copyright 2013, Qualcomm Innovation Center, Inc.
 *
 *    All rights reserved.
 *    This file is licensed under the 3-clause BSD license in the NOTICE.txt
 *    file for this project. A copy of the 3-clause BSD license is found at:
 *
 *        http://opensource.org/licenses/BSD-3-Clause.
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the license is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the license for the specific language governing permissions and
 *    limitations under the license.
 ******************************************************************************/
#ifndef _ABOUTSERVICE_H
#define _ABOUTSERVICE_H


#include <vector>
#include <map>
#include <alljoyn/BusObject.h>
#include <qcc/String.h>
#include "PropertyStore.h"
namespace ajn {
namespace services {

/**
 * AboutService is an AllJoyn BusObject that implements the org.alljoyn.About standard interface.
 * Applications that provide AllJoyn IoE services use an instance of this class to announce
 * their capabilities and other identifying details of the services being provided.
 */
class AboutService : public ajn::BusObject {
  public:
    /**
     * Construct an AboutService.
     * @param[in]  bus    BusAttachment instance associated with this AboutService
     * @param[in]  store  Set of key/value pairs used to populate required/optional/app-defined
     *               members of about and announce data
     */
    AboutService(ajn::BusAttachment& bus, PropertyStore& store);
    /**
     *
     */
    ~AboutService() { }

    /**
     * Register the AboutService on the AllJoyn bus passing the port to be announced.
     * @param port
     * @return status.
     */
    QStatus Register(int port);
    /**
     *
     */
    void Unregister();

    /**
     * AddObjectDescription adds objects Description to the AboutService announcement.
     * @param[in]  path of the interface.
     * @param[in]  interfaceNames
     * @return ER_OK if successful.
     */
    QStatus AddObjectDescription(qcc::String path, std::vector<qcc::String> interfaceNames);
    /**
       *RemoveObjectDescription adds objects Description to the AboutService announcement.
     * @param[in]  path of the interface.
     * @param[in]  interfaceNames
     * @return ER_OK if successful.
     */
    QStatus RemoveObjectDescription(qcc::String path, std::vector<qcc::String> interfaceNames);

    /**
     * Send or replace the org.alljoyn.About.Announce sessionless signal.
     *
     * Validate store and object announcements and emit the announce signal.
     *
     * @return
     * - ER_MANDATORY_FIELD_MISSING: Logs an error with specific field that has a problem.
     */
    QStatus Announce();

    /**
     * Add BusObjects descriptions to the AboutService.
     *
     * @param obj   Bus object to be announced by this AboutService
     */
    void AddObjectDescription(const ajn::BusObject& obj);
    void RemoveObjectDescription(const ajn::BusObject& obj);


  private:

    /**
     *	pointer to BusAttachment
     */
    ajn::BusAttachment* myBusAttachment;


    /**
     * port published  by the announcement
     */
    int announcePort;

    /**
     *	stores the signal member initialized  in the Register(..)
     */
    const ajn::InterfaceDescription::Member* announceSignalMember;

    /**
     * Handles  GetAboutData method
     * @param[in]  member
     * @param[in]  msg reference of AllJoyn Message
     */
    void GetAboutDataHandler(const ajn::InterfaceDescription::Member* member, ajn::Message& msg);

    /**
     *	Handles  GetObjectDescription method
     * @param[in]  member
     * @param[in]  msg reference of AllJoyn Message
     */
    void GetObjectDescriptionHandler(const ajn::InterfaceDescription::Member* member, ajn::Message& msg);



    /**
     * Handles the GetPropery request
     * @param[in]  ifcName  interface name
     * @param[in]  propName the name of the properly
     * @param[in]  val reference of MsgArg out parameter.
     * @return ER_OK if successful.
     */
    QStatus Get(const char*ifcName, const char*propName, MsgArg& val);

    /**
     *		map that
     */
    std::map <qcc::String, std::vector<qcc::String> > controlInterfaceMap;



    /**
     *	pointer to PropertyStore
     */
    PropertyStore* myPropertyStore;


};

}
}

#endif /*_ABOUTSERVICE_H*/
